
# Tap CryptoCompare

A [Singer tap](https://github.com/singer-io/getting-started) for loading data from [CrytpoCopare's API](https://www.cryptocompare.com).


This codebase is heavily influenced by https://github.com/singer-io/tap-exchangeratesapi. Thanks for the great example!

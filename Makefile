
all: movetostore

sample:
	python3 tapcryptocompare/main.py --config config.exm.json | target-csv -c target_csv_config.exm.json

movetostore: sample
	# Temporary hack until I can submit a PR to `target-csv` to allow the config file to dictate where
	# to save.
	mv *.csv datastore

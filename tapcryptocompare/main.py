
"""
    This tap heavily inspired by https://github.com/singer-io/tap-exchangeratesapi
"""

import argparse

import singer

from config import Config
from state import State
from sync import sync


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-c', '--config', help='Config file', required=False)
    parser.add_argument(
        '-s', '--state', help='State file', required=False)
    args = parser.parse_args()

    config = Config.from_file(args.config) if args.config else Config()
    state = State.from_file(args.state) if args.state else State()

    sync(config, state)


if __name__ == "__main__":
    main()
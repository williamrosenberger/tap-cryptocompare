
"""
    Thanks to https://github.com/singer-io/tap-exchangeratesapi
    for a lot of the inspiration behind this code.
"""

import itertools

import requests
import backoff
import singer

from schema import Schema


log = singer.get_logger()


def giveup(error):
    """
        Decides whether to give up on this query.
    """
    response = error.response
    return not (
        response.status_code == 429 or
        response.status_code >= 500
    )


def make_getter(base_url, **params):
    """
        Creates a function for making the given query.

        Thanks to https://github.com/singer-io/tap-exchangeratesapi
        for a lot of the inspiration behind this code.
    """
    @backoff.on_exception(
        backoff.constant,
        requests.exceptions.RequestException,
        jitter=backoff.random_jitter,
        max_tries=5,
        giveup=giveup,
        interval=30
    )
    def get():
        response = requests.get(url=base_url, params=params)
        response.raise_for_status()
        return response

    return get


def tag_entries_with_meta(entries, meta):
    """
        Because we're iterating over multiple sources (multiple "to" and "from" pairs),
        we need to add some extra meta data to each of the sections. This is a helper
        method that will walk over an iterable of dictionaries and add the fields in `meta`
        to each record.
    """
    for entry in entries:
        entry.update(meta)
        yield entry


def sync(config, state):
    """
        Reads in new data.
    """
    log.info("Synchronizing from {}".format(state.start_date))
    log.info("Getting data from {}".format(config.request_from))

    singer.write_schema('cryptocompare-histominute', Schema.schema_json(), key_properties=Schema.schema_key_properties())

    # Make a query for every pair specified in the config. Concatenate
    # the results into a single iterable.
    queries = (
        make_getter(config.request_from, fsym=pair["fsym"], tsym=pair["tsym"]) for pair in config.pairs
    )
    responses = (query() for query in queries)
    results = (response.json() for response in responses)
    data_sections = (
        tag_entries_with_meta(res["Data"], pair) for res, pair in zip(results, config.pairs)
    )
    all_data = itertools.chain.from_iterable(data_sections)

    # Parse the incomming data.
    data_objs = (Schema.from_json(data) for data in all_data)
    parsed_data = (Schema.to_json(obj) for obj in data_objs)

    # And print it out to singer.
    singer.write_records('cryptocompare-histominute', parsed_data)

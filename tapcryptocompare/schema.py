
from datetime import datetime

import singer

log = singer.get_logger()


class Schema():
    """
        Defines the attributes that are part of the objects returned from the API.
    """

    @classmethod
    def schema_json(cls):
        """
            Returns JSON describing this schema.
        """

        return {
            "properties": {
                "time": {
                    "type": "string",
                    "format": "date-time"
                },
                "open": {
                    "type": "number",
                },
                "high": {
                    "type": "number",
                },
                "low": {
                    "type": "number",
                },
                "close": {
                    "type": "number",
                },
                "volumefrom": {
                    "type": "number",
                },
                "volumeto": {
                    "type": "number",
                },
                "fsym": {
                    "type": "string"
                },
                "tsym": {
                    "type": "string"
                }
            }
        }

    @classmethod
    def schema_key_properties(cls):
        """
            Returns the names of the properties that make up this schema's primary key.
        """
        return ["fsym", "tsym", "time"]

    @classmethod
    def from_json(cls, data):
        """
            Instantiates this class based on the given JSON.
        """
        if 'time' in data:
            # Currently only support conversion from unix timestamp. Feel free to implement
            # additional checks here.
            time = datetime.utcfromtimestamp(data.pop('time'))

        return cls(**data, time=time)

    def __init__(self, time, open, high, low, close, volumefrom, volumeto, fsym, tsym):
        self.time = time
        self.open = open
        self.high = high
        self.low = low
        self.close = close
        self.volumefrom = volumefrom
        self.volumeto = volumeto
        self.fsym = fsym
        self.tsym = tsym

    def to_json(self):
        return {
            'time': self.time.isoformat(),
            'open': self.open,
            'high': self.high,
            'low': self.low,
            'close': self.close,
            'volumefrom': self.volumefrom,
            'volumeto': self.volumeto,
            'fsym': self.fsym,
            'tsym': self.tsym,
        }

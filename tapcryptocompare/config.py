
import json


class Config():
    """
        Defines how to interpret the configuration options for this tap.
    """

    @classmethod
    def from_file(cls, path):
        """
            Instantiates this class based on contents of a file.
        """
        with open(path) as f:
            raw_conf = json.load(f)

        return cls.from_json(raw_conf)

    @classmethod
    def from_json(cls, json_obj):
        """
            Instantiates this class based on JSON data.
        """
        return cls(**json_obj)

    def __init__(self, request_from=None, pairs=[]):
        """
            `request_from` is the base URL that should be used for the API.
            `pairs` is a list of dictionaries where each dict defines the `from` and `to` units
            that should be watched.
        """
        self.pairs = pairs
        self.request_from = request_from

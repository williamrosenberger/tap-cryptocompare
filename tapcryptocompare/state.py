
from datetime import datetime

import singer


class State():
    """
        Defines the state that this tap records.
    """

    @classmethod
    def from_file(cls, path):
        """
            Instantiates this class based on contents of a file.
        """
        with open(path) as f:
            raw_conf = json.load(f)

        return cls.from_json(raw_conf)

    @classmethod
    def from_json(cls, json_obj):
        """
            Instantiates this class based on JSON data.
        """
        if 'start_date' in json_obj:
            raw_date = json_obj.pop('start_date')
            start_date = singer.utils.strptime_with_tz(raw_date).date().strftime(DATE_FORMAT)

        return cls(**json_obj, start_date=start_date)

    def __init__(self, start_date=datetime.min):
        """
            `start_date` is a `datetime` describing the earliest data that should be recorded.
                Defaults to the oldest possible `datetime` (meaning all data returned by the source
                will be recorded).
        """
        self.start_date = start_date
